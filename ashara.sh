#!/bin/bash
#INSTALATION SCRIPT

# 1. PREREQUISITES (gpg keys, sources and PPAs)
sudo apt-get update
sudo apt-get install -y curl software-properties-common
# Visual Studio Code
sudo sh -c 'curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor >  /etc/apt/trusted.gpg.d/microsoft.gpg'
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
#Spotify
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
sudo sh -c 'echo "deb http://repository.spotify.com stable non-free" > /etc/apt/sources.list.d/spotify.list'
#Numix-Circle
sudo add-apt-repository -y ppa:numix/ppa
#Elementary Tweaks
sudo add-apt-repository -y ppa:philip.scott/elementary-tweaks

# 2. UPDATE
sudo apt update

# 3. INSTALLS...
DiscordDEB="/tmp/discord.deb"
curl -L "https://discordapp.com/api/download?platform=linux&format=deb" -o $DiscordDEB

sudo apt install -y firefox apt-transport-https code psensor spotify-client apt zsh git numix-icon-theme-circle elementary-tweaks zsh guake gnome-system-monitor fonts-powerline $DiscordDEB libreoffice

rm $DiscordDEB
unset DiscordDEB


#UNINSTALL EPIPHANY
sudo apt remove -y epiphany-browser epiphany-browser-data epiphany-extensions

#INSTALL OH MY ZSH
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

if [ ! -d "~/.config/fontconfig/" ]; then
mkdir -p ~/.config/fontconfig/
fi
# Inicializar conf.d:
tee ~/.config/fontconfig/conf.d << 'EOF'
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
  <selectfont>
    <acceptfont>
      <pattern>
        <patelt name="family"><string>terminess powerline</string></patelt>
      </pattern>
    </acceptfont>
  </selectfont>
</fontconfig>
EOF

tempFUENTES="/tmp/fuentes"
git clone -q https://github.com/powerline/fonts.git $tempFUENTES --depth=1
$tempFUENTES/install.sh
rm -rf $tempFUENTES
unset tempFUENTES
fc-cache -vf
