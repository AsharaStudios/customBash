#!/bin/bash

# Script para personalizar la consola en elementary OS
# de tal manera que al estar dentro de un proyecto Git
# se muestre la rama y estado actual del repositorio.

echo ""
echo "Voy a instalarte elementary-tweaks, para que puedas"
echo "personalizar tu elementary desde el panel de control"
echo "y tambien las fuentes que permitiran ver el control"
echo "de versiones re nitido en la consola"
echo ""
sudo add-apt-repository -y ppa:philip.scott/elementary-tweaks
sudo apt update
sudo apt install -y fonts-powerline elementary-tweaks

# Acorde al readme de https://github.com/powerline/fonts
# Si no existe el directorio de configuracion de fuentes, crearlo...
if [ ! -d "~/.config/fontconfig/" ]; then
mkdir ~/.config/fontconfig/
fi
# .. y acceder al directorio
cd ~/.config/fontconfig/
# ... para crear el archivo conf.d con lo siguiente:
tee conf.d << 'EOF'
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
  <selectfont>
    <acceptfont>
      <pattern>
        <patelt name="family"><string>terminess powerline</string></patelt>
      </pattern>
    </acceptfont>
  </selectfont>
</fontconfig>
EOF

cd /tmp
# clone
git clone https://github.com/powerline/fonts.git --depth=1
# install
fonts/install.sh
# clean-up a bit
rm -rf fonts

# Refrescar cache de fuentes
fc-cache -vf

# Preparar al usuario y Lanzar panel de configuracion
echo ""
echo "Abriré el panel de configuracion por ti."
echo "Cuando abra, ve a 'Tweaks' y en la columna de la"
echo "izquieda, escoge 'Fonts' ... 'Tipos de Letra', o"
echo "segun tu idioma, la segunda opcion posiblemente"
echo "tiene dos letras A de color negro y rojo."
echo "Para la tercer categoría (Monospace font) presiona"
echo "el boton para que puedas escoger una fuente."
echo "Ingresa en el campo de busqueda las palabras"
echo "'mono powerline' para que solo aparezcan las"
echo "fuentes monoespaciadas compatibles con PowerLine;"
echo "selecciona la que mas te guste, y presiona la tecla"
echo "'enter' para confirmar tu seleccion y cerrar el selector"
echo "finalmente cierra el panel de configuracion y"
echo "también esta terminal. Todo debería de estar listo."
echo ""
echo "Podrás ver estas instrucciones moviendo el panel de"
echo "configuracion. Presiona Enter para empezar"
echo ""
read -n 1
switchboard
