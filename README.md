# Scripts de configuracion de Estado Git en Terminal

Se da una capa de personalizacion a la terminal para que detecte si se encuentra en un repositorio Git y en este caso, una breve indicación del estado del repositorio.

Se toma como base una configuración básica para el proyecto [oh my zsh](https://github.com/robbyrussell/oh-my-zsh/), la herramienta de personalización de elementary, [elementary tweaks](https://github.com/elementary-tweaks/elementary-tweaks); y las [fuentes powerline](https://github.com/powerline/fonts) necesarias para que todo funcione al peluche, llegando a algo similar a:

![Terminal de elementary, con la personalizacion debida al script](terminal-elementary.png)

## Script Ashariano

Instala varios programas que se usan en Ashara. (Wow... Que creativo)

1. Descargar y ejecutar el script. En un terminal, ingresar la siguiente línea:

   ```bash
   sh -c "$(wget https://gitlab.com/AsharaStudios/customBash/raw/ashara/ashara.sh?inline=false -O -)"
   ```

   **NOTA:** Al final de la instalación vuelve a pedir nuevamente la contraseña, para dejar zsh como el programa de consola por defecto.

2. Personalizar la instalacion de OhMyZsh. En la terminal (puede ser la misma que se hizo el anterior paso), ejecutar `nano ~/.zshrc`, lo cual abre *nano*, un editor de texto plano presente en la mayoría de distribuciones GNU/Linux (puedes ver [este tutorial](https://www.comoinstalarlinux.com/como-usar-el-editor-nano-linux/) si no tienes ni idea de qué te estoy hablando).

    1. Buscar la linea que empieza por

       ```bash
       ZSH_THEME=...
       ```

       y dejarla como

       ```bash
       ZSH_THEME="agnoster"
       ```

    2. Buscar el grupo de lineas similar a

       ```bash
       plugins=(
           git
       )
       ```

       y completarlo para que quede como

       ```bash
       plugins=(
           git
           bundler
           dotenv
           osx
           rake
           rbenv
           ruby
       )
       ```

    3. Guardar el archivo presionando <kbd>Ctrl</kbd> + <kbd>X</kbd> y luego <kbd>Y</kbd>. Hasta acá se ha hecho una personalización básica, pero se pueden modificar otros parámetros, de acuerdo a los mismos comentarios del archivo `~/.zshrc`.

3. Aplicar una fuente Powerline para la terminal. En el menu de Aplicaciones escribe "tweaks", para abrir el panel de Configuración de Elementary Tweaks (instalado por el script anterior 😉 )

   ![Ilustración de los 4 pasos siguientes, con la interfaz en Español](tweaks.png)

   1. Escoger la segunda categoría a la izquierda, "Tipos de Letra".
   2. Seleccionar la fuente para la terminal, "Monospace Font".
   3. Filtrar las fuentes escribiendo "mono powerline" en el campo de búsqueda y escoger alguna de las que aparecen con este filtro.
   4. Guardar cambios.

   Despues de lo anterior, al abrir un nuevo terminal debería aparecer como la imagen al comienzo de este documento.
